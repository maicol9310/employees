﻿using Domain.Entities;
using System.Collections.Generic;

namespace Application.Application.Services.Employe
{
    public interface IEmployeService
    {
        void Create(Employees employeesEntitie);
        void Update(Employees employeesEntitie);
        void Delete(int id);
        List<Employees> GetEmployees();
    }
}
