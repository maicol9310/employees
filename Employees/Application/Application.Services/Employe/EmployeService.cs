﻿using Domain.Entities;
using DomainCotract = Domain.Repositories.Contracts;
using System.Collections.Generic;

namespace Application.Application.Services.Employe
{
    public class EmployeService: IEmployeService
    {
        readonly DomainCotract.IEmployeesRepository _domainICotract;

        public EmployeService(DomainCotract.IEmployeesRepository domainICotract)
        {
            this._domainICotract = domainICotract;
        }

        public void Create(Employees employeesEntitie)
        {
            _domainICotract.Create(employeesEntitie);
        }

        public void Update(Employees employeesEntitie)
        {
            _domainICotract.Update(employeesEntitie);
        }

        public void Delete(int id)
        {
            _domainICotract.Delete(id);
        }

        public List<Employees> GetEmployees()
        {
            return _domainICotract.GetEmployees();
        }
    }
}
