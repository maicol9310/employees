﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Employees
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Office { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string EmployeeCode { get; set; }

        [Column(TypeName = "nvarchar(150)")] 
        public string EmployeeName { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string EmployeeSurname { get; set; }

        public int Division { get; set; }
        public int Position { get; set; }
        public int Grade { get; set; }

        [Column(TypeName = "date")]
        public DateTime BeginDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string IdentificationNumber { get; set; }

        [Column(TypeName = "decimal(18, 0)")]
        public decimal BaseSalary { get; set; }

        [Column(TypeName = "decimal(18, 0)")]
        public decimal ProductionBonus { get; set; }

        [Column(TypeName = "decimal(18, 0)")]
        public decimal CompensationBonus { get; set; }

        [Column(TypeName = "decimal(18, 0)")]
        public decimal Commission { get; set; }

        [Column(TypeName = "decimal(18, 0)")]
        public decimal Contributions { get; set; }
    }
}
