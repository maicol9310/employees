﻿using Application.Application.Services.Employe;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Domain.Entities;

namespace api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeService _iEmployeService;

        private readonly ILogger<EmployeesController> _logger;

        public EmployeesController(IEmployeService iEmployeService, ILogger<EmployeesController> logger)
        {
            this._iEmployeService = iEmployeService;
            _logger = logger;
        }

        [HttpGet]
        public List<Employees> Get()
        {
            List<Employees> response = new List<Employees>();
            try
            {
                response = _iEmployeService.GetEmployees();

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Employees employeesEntitie)
        {
            try
            {
                Employees _employeesEntitie = new Employees();
                _employeesEntitie = employeesEntitie;
                _employeesEntitie.Id = id;
                _iEmployeService.Update(_employeesEntitie);
            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }

        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                _iEmployeService.Delete(id);
            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
        }

        [HttpPost]
        public void Post([FromBody] Employees employeesEntitie)
        {
            try
            {
                _iEmployeService.Create(employeesEntitie);
            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
        }
    }
}
