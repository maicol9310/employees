﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using System.Linq;
using Domain.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{

    public class EmployeesRepository: IEmployeesRepository
    {
        private EmployeesDbContext _context;

        public EmployeesRepository(EmployeesDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Create(Employees employeesEntitie)
        {
            _context.EmployeesS.Add(employeesEntitie);
            _context.SaveChanges();
        }

        public void Update(Employees employeesEntitie)
        {
            var employee = _context.EmployeesS.Find(employeesEntitie.Id);
            employee.Id = employeesEntitie.Id;
            employee.Year = employeesEntitie.Year;
            employee.Month = employeesEntitie.Month;
            employee.Office = employeesEntitie.Office;
            employee.EmployeeCode = employeesEntitie.EmployeeCode;
            employee.EmployeeName = employeesEntitie.EmployeeName;
            employee.EmployeeSurname = employeesEntitie.EmployeeSurname;
            employee.Division = employeesEntitie.Division;
            employee.Position = employeesEntitie.Position;
            employee.Grade = employeesEntitie.Grade;
            employee.BeginDate = employeesEntitie.BeginDate;
            employee.Birthday = employeesEntitie.Birthday;
            employee.IdentificationNumber = employeesEntitie.IdentificationNumber;
            employee.BaseSalary = employeesEntitie.BaseSalary;
            employee.ProductionBonus = employeesEntitie.ProductionBonus;
            employee.CompensationBonus = employeesEntitie.CompensationBonus;
            employee.Commission = employeesEntitie.Commission;
            employee.Contributions = employeesEntitie.Contributions;

            _context.EmployeesS.Update(employee);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.EmployeesS.Remove(_context.EmployeesS.Find(id));
            _context.SaveChanges();
        }

        public List<Employees> GetEmployees()
        {
            var query =
                from employees in _context.EmployeesS
                select employees;
            var x = query.ToList();
            return _context.EmployeesS.ToList();

        }
    }
}
