﻿namespace Infrastructure
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;

    public class EmployeesDbContext: DbContext
    {
        public EmployeesDbContext(DbContextOptions<EmployeesDbContext> options) : base(options) { }

        public DbSet<Employees> EmployeesS { get; set; }

        public DbSet<Office> Offices { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<Division> Divisions { get; set; }

    }
}
