﻿using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Repositories.Contracts
{
    public interface IEmployeesRepository
    {
        void Create(Employees employeesEntitie);
        void Update(Employees employeesEntitie);
        void Delete(int id);
        List<Employees> GetEmployees();
    }
}
