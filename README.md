# README #

### Pasos para ejecutar este proyecto ###

* Elimine la carpeta Migration de Infrastructure
* Para realizar emigración de la base de datos, crear una base de datos y cambiar el string de conexión en presentation\api\appsettings.json . Luego ubiquese en la ruta presentation\api y ejecute los siguientes comando en una consola de comando:
* dotnet ef migrations add EmployeesDB -p ..\..\Infrastructure\Infrastructure.csproj
* dotnet ef database update EmployeesDB -p ..\..\Infrastructure\Infrastructure.csproj
* Run al proyecto de inicio en Presentation/api

### Pruebe la api con Swagger de OpenApi integrado en el proyecto o con PostMan ###