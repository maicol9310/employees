export interface listEmployeesI{
    id: number;
    year: number;
    month: number;
    office: number;
    employeeCode: string;
    employeeName: string;
    employeeSurname: string;
    division: number;
    position: number;
    grade: number;
    beginDate: Date;
    birthday: Date;
    identificationNumber: string;
    baseSalary: number;
    productionBonus: number;
    compensationBonus: number;
    commission: number;
    contributions: number;
}