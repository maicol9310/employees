import { Injectable } from '@angular/core';
import {listEmployeesI} from '../../models/listEmployees.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
import {responseI} from '../../models/response.interface'

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  
  constructor(private http:HttpClient) { }

  ListEmployees():Observable<listEmployeesI[]>{
    let dir = "https://localhost:44352/api/Employees";
    return this.http.get<listEmployeesI[]>(dir);
  }

  GetSingleEmployees(id:any):Observable<listEmployeesI>{
    let dir = "https://localhost:44352/api/Employees/" + id;
    return this.http.get<listEmployeesI>(dir);
  }

  PutEmployees(id:any, form:listEmployeesI):Observable<responseI>{
    let dir = "https://localhost:44352/api/Employees/" + id;
    return this.http.put<responseI>(dir, form);
  }
}