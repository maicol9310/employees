import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import { Router } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';

import { listEmployeesI } from '../../models/listEmployees.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})

export class ListComponent implements OnInit {

  listemployees:listEmployeesI[];

  constructor(private api:ApiService, private router:Router) { }

  ngOnInit(): void {   
    this.api.ListEmployees().subscribe(data =>{
      this.listemployees = data as listEmployeesI[];
      console.log(this.listemployees);
    })
  } 

  editEmploye(id:any){
    this.router.navigate(['edit',id]);
  }

  newEmploye(){
    this.router.navigate(['new']);
  }

}


